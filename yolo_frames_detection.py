import numpy as np
import time
import cv2
import os
import glob

# https://cloudxlab.com/blog/object-detection-yolo-and-python-pydarknet/

INPUT_FOLDER='m2cai_results/test'
OUTPUT_FOLDER='m2cai_results/results'

# Detector settings
LABELS_FILE='m2cai_results/obj_m2cai.names'
CONFIG_FILE='m2cai_results/yolov4_m2cai.cfg'
WEIGHTS_FILE='m2cai_results/yolov4_m2cai_best.weights'
CONFIDENCE_THRESHOLD=0.25




images_path = glob.glob(r"" + INPUT_FOLDER +"/*.jpg")

LABELS = open(LABELS_FILE).read().strip().split("\n")

np.random.seed(4)
COLORS = np.random.randint(0, 255, size=(len(LABELS), 3),
	dtype="uint8")


net = cv2.dnn.readNetFromDarknet(CONFIG_FILE, WEIGHTS_FILE)

for img_path in images_path:

	image = cv2.imread(img_path)
	(H, W) = image.shape[:2]

	# determine only the *output* layer names that we need from YOLO
	ln = net.getLayerNames()
	ln = [ln[i - 1] for i in net.getUnconnectedOutLayers()]


	blob = cv2.dnn.blobFromImage(image, 1 / 255.0, (416, 416),
		swapRB=True, crop=False)
	net.setInput(blob)
	start = time.time()
	layerOutputs = net.forward(ln)
	end = time.time()


	print("[INFO] YOLO took {:.6f} seconds".format(end - start))


	# initialize our lists of detected bounding boxes, confidences, and
	# class IDs, respectively
	boxes = []
	confidences = []
	classIDs = []

	bbox_coordinates = []

	# loop over each of the layer outputs
	for output in layerOutputs:
		# loop over each of the detections
		for detection in output:
			# extract the class ID and confidence (i.e., probability) of
			# the current object detection
			scores = detection[5:]
			classID = np.argmax(scores)
			confidence = scores[classID]

			# filter out weak predictions by ensuring the detected
			# probability is greater than the minimum probability
			if confidence > CONFIDENCE_THRESHOLD:
				# scale the bounding box coordinates back relative to the
				# size of the image, keeping in mind that YOLO actually
				# returns the center (x, y)-coordinates of the bounding
				# box followed by the boxes' width and height
				box = detection[0:4] * np.array([W, H, W, H])
				(centerX, centerY, width, height) = box.astype("int")

				left, top, right, bottom = int(centerX - (width / 2)), int(centerY + (height / 2)), int(centerX + (width / 2)), int(centerY - (height / 2))

				bbox_coordinates.append([centerX, centerY, left, top, right, bottom])

				# use the center (x, y)-coordinates to derive the top and
				# and left corner of the bounding box
				x = int(centerX - (width / 2))
				y = int(centerY - (height / 2))

				# update our list of bounding box coordinates, confidences,
				# and class IDs
				boxes.append([x, y, int(width), int(height)])
				confidences.append(float(confidence))
				classIDs.append(classID)

	# apply non-maxima suppression to suppress weak, overlapping bounding
	# boxes
	idxs = cv2.dnn.NMSBoxes(boxes, confidences, CONFIDENCE_THRESHOLD,
		CONFIDENCE_THRESHOLD)

	# ensure at least one detection exists
	if len(idxs) > 0:
		# loop over the indexes we are keeping

		# create .txt file
		f = open(OUTPUT_FOLDER + "/" + os.path.basename(img_path).split('.')[0] + ".txt", "w+")

		for i in idxs.flatten():
			# write bounding box coordinates to .txt file
			(centx, centy, le, to, ri, bo) = (bbox_coordinates[i][0], bbox_coordinates[i][1], bbox_coordinates[i][2], bbox_coordinates[i][3],
			bbox_coordinates[i][4], bbox_coordinates[i][5])
			f.write(str(classIDs[i]) + " " + str(centx) + " " + str(centy) + " " + str(le) + " " + str(to) + " " + str(
				ri) + " " + str(bo) + '\n')

			# draw bounding box
			(x, y) = (boxes[i][0], boxes[i][1])
			(w, h) = (boxes[i][2], boxes[i][3])

			color = [int(c) for c in COLORS[classIDs[i]]]

			cv2.rectangle(image, (x, y), (x + w, y + h), color, 2)
			text = "{}: {:.4f}".format(LABELS[classIDs[i]], confidences[i])
			cv2.putText(image, text, (x, y - 5), cv2.FONT_HERSHEY_SIMPLEX,
				0.5, color, 2)

		f.close()

	# show the output image
	cv2.imwrite(OUTPUT_FOLDER + "/" + os.path.basename(img_path).split('.')[0] + ".jpg", image)